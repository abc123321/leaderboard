package db

 import (
	 "context"
	 "errors"
	 "github.com/go-redis/redis"
 )

 type Database struct{
	 Client *redis.Client
 }

 var (
	ErrNil = errors.New("no matching record found in redis database")
	Ctx    = context.TODO()
 )
 func newDatabase(address string) (*Database, error){
	 Client := redis.NewClient(&redis.Options{
		 Addr: address,
		 Password: "",
		 DB: 0,
	 })
	 err := Client.Ping().Err();
	 if err != nil {
		return nil, err
	 }

	 return &Database{
		 Client: Client,
	 }, ErrNil
 }